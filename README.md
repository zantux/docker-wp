0. this is need docker and docker-compose to be already installed
1. clone this repo & enter di directory ex : "cd docker-wp"
2. edit file .env
3. set below variable to .env file : <br />
    MARIADB_ROOT_PASSWORD: "your DB root user pass"<br />
    MARIADB_DATABASE: "your wp db name"<br />
    MARIADB_USER: "your wp db username"<br />
    MARIADB_PASSWORD: "your wp db username"<br />
4. there is example files already created for reference
5. run this command "docker-compose pull"
6. run this command "docker-compose up -d"
